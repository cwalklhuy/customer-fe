import React, {Component} from 'react';
import './App.css';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      username: null,
      password: null,
      login: false,
      store: null
    }
  }

  componentDidMount() {
    this.storeCollector()
  }

  storeCollector() {
    let store = JSON.parse(localStorage.getItem('login'));
    if (store && store.login) {
      this.setState({login: true, store: store.token})
    }
  }

  login() {
    fetch('http://localhost:8084/api/users/signin', {
      method: "POST",
      body: JSON.stringify(this.state),
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json"
      }
    }).then(response => {
      response.json().then(result => {
        //
        console.warn('result', result.token);
        localStorage.setItem('login', JSON.stringify({
          login: true,
          token: result.token
        }))
        this.storeCollector()
      })
    })
  }

  post() {
    let token = 'Bearer ' + this.state.store;
    console.log(token);
    fetch('http://localhost:8084/api/test/user', {
      method: "GET",
      // body: JSON.stringify(this.state),
      headers: {
        "Authorization": token,
        "Accept": "application/json",
        "Content-Type": "application/json"
      }
    }).then(response => {
      // console.log(response);
      response.json().then(result => {
        console.warn('result', result.aaa);
      })
    })
  }

  render() {
    return (
      <div className="App">
        <div>
          <h1>Login</h1>
          {
            !this.state.login?
              <div>
                <input type="text" onChange={(event) => this.setState({username: event.target.value})}/><br/>
                <input type="password"
                       onChange={(event) => this.setState({password: event.target.value})}/><br/>
                <button onClick={() => {
                  this.login()
                }}>Login
                </button>
              </div>
              :
              <div>
                <textarea onChange={(event) => this.setState({post: event.target.value})}/>
                <button onClick={() => this.post()}>Post</button>
              </div>
          }
        </div>
      </div>
    );
  }


}

export default App;
