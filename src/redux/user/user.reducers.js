//Để đẩy dữ liệu vào store
//Có chứa model trong này
import UserActionTypes from "./user.types";

const INITIAL_STATE = {
  token: ""
}

const userReducer = (state=INITIAL_STATE, action) => {
  switch (action.type) {
    case UserActionTypes.LOGIN:
      return {
        //Gán object mới để react doom kiểm tra
        ...state,
        token: action.payload
      };
    default: return state;
  }
}
//state.token = action.payload
//viết kiểu này react doom ko render component
